package com.check24.interviewTasks.apiModels.createUser;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@Builder
@Getter
public class UserRequest implements Serializable {

	@JsonProperty("name")
	private String name;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("email")
	private String email;

	@JsonProperty("status")
	private String status;
}