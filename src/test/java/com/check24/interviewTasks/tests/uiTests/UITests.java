package com.check24.interviewTasks.tests.uiTests;

import io.github.bonigarcia.wdm.WebDriverManager;
import com.check24.interviewTasks.pages.EinsurancePkwBerechnenPage;
import com.check24.interviewTasks.pages.HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class UITests {
    private final String UploadFileName = "TestAutomationTask-Naim.pdf" ;
    private final String FilePathToUpload = System.getProperty("user.dir")+"/resources/"+UploadFileName+"";
    WebDriver driver;
    HomePage homePage;
    EinsurancePkwBerechnenPage einsurancePkwBerechnenPage;
    @BeforeSuite
    public void beforeSuiteSetUp(){
        WebDriverManager.chromedriver().setup();
    }

    @BeforeMethod
    public void beforeClassSetUp(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("https://www.check24.de/kfz-versicherung/");
        homePage = new HomePage(driver);
    }

    @Test
    public void executeAllStepsTest(){
        homePage.clickAkzeptierenButton();
        homePage.clickWechselnPkwRadioInput();
        einsurancePkwBerechnenPage = homePage.clickTarifeVergleichenButton();
        einsurancePkwBerechnenPage.clickAndernSpan();
        einsurancePkwBerechnenPage.clickWeiterButton();
        einsurancePkwBerechnenPage.uploadAktuelenVersicherungHochladenInput(FilePathToUpload);
        einsurancePkwBerechnenPage.clickJetztHochladenUndAnalysierenButton();
        einsurancePkwBerechnenPage.clickNeinMitVorhandenemDokumentWeiterButton();
        einsurancePkwBerechnenPage.clickOhneAnmeldungFortfahrenButton();
    }

    @Test
    public void checkUserCannotUploadSameFileTwice(){
        homePage.clickAkzeptierenButton();
        homePage.clickWechselnPkwRadioInput();
        einsurancePkwBerechnenPage = homePage.clickTarifeVergleichenButton();
        einsurancePkwBerechnenPage.clickAndernSpan();
        einsurancePkwBerechnenPage.clickWeiterButton();
        einsurancePkwBerechnenPage.uploadAktuelenVersicherungHochladenInput(FilePathToUpload);
        einsurancePkwBerechnenPage.uploadAktuelenVersicherungHochladenInput(FilePathToUpload);

        Assert.assertEquals(einsurancePkwBerechnenPage.getNumberOfSameNameUploadedFiles(UploadFileName),1);
    }

    @Test
    public void checkFileName_isEqualWith_UploadedFileNameDisplayed(){
        homePage.clickAkzeptierenButton();
        homePage.clickWechselnPkwRadioInput();
        einsurancePkwBerechnenPage = homePage.clickTarifeVergleichenButton();
        einsurancePkwBerechnenPage.clickAndernSpan();
        einsurancePkwBerechnenPage.clickWeiterButton();
        einsurancePkwBerechnenPage.uploadAktuelenVersicherungHochladenInput(FilePathToUpload);

        Assert.assertEquals(einsurancePkwBerechnenPage.getUploadedFileNameElementText(),UploadFileName);
    }

    @AfterMethod
    public void afterClassTearDown(){
        driver.quit();
    }

}
