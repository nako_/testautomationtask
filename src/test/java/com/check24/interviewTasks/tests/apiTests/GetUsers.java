package com.check24.interviewTasks.tests.apiTests;

import com.check24.interviewTasks.apiModels.createUser.UserRequest;
import org.testng.annotations.*;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.oauth2;

public class GetUsers extends BaseTest{
    UserRequest user;
    int userId;
    public GetUsers() {
        this.user = UserRequest.builder()
                .name("User 02")
                .gender("male")
                .email("random02@email.com")
                .status("active")
                .build();
    }

    @BeforeClass
    public void beforeClassSetUp(){
        userId = given().
                spec(requestSpec).
                when().
                body(user).
                post("/users").
                then().
                statusCode(201).
                extract().path("id");
    }

    @Test
    public void getUserDetails_checkResponseCode_expect200() {
        given().
                spec(requestSpec).
        when().
                get("/users/"+userId).
        then().
                statusCode(200);
    }

    @AfterClass
    public void deleteUser(){
        given().
                spec(requestSpec).
        when()
                .delete("/users/"+userId);
    }
}
