package com.check24.interviewTasks.tests.apiTests;

import com.check24.interviewTasks.apiModels.createUser.UserRequest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class CreateUser extends BaseTest{
    int userId;
    UserRequest user;

    public CreateUser() {
        this.user = UserRequest.builder()
                .name("User 01")
                .gender("male")
                .email("random01@email.com")
                .status("active")
                .build();
    }

    @Test
    public void createUser_checkResponseCode_expect201() {
        userId = given().
                spec(requestSpec).
        when().
                body(user).
                post("/users").
        then().
                statusCode(201).
                extract().path("id");
    }

    @Test
    public void createUser_checkResponseName_expectUser01() {
        userId = given().
                spec(requestSpec).
        when().
                body(user).
                post("/users").
        then().
                spec(responseSpec).
                body("name",equalTo(user.getName())).
                extract().path("id");
    }

    @Test
    public void createUser_checkResponseEmail_expectUserEmail() {
        userId = given().
                spec(requestSpec).
                when().
                body(user).
                post("/users").
                then().
                spec(responseSpec).
                body("email",equalTo(user.getEmail())).
                extract().path("id");
    }

    @Test
    public void createUser_checkGender_expectFemale() {
        userId = given().
                spec(requestSpec).
                when().
                body(user).
                post("/users").
                then().
                spec(responseSpec).
                body("gender",equalTo(user.getGender())).
                extract().path("id");
    }

    @Test
    public void createUser_checkStatus_expectActive() {
        userId = given().
                spec(requestSpec).
                when().
                body(user).
                post("/users").
                then().
                spec(responseSpec).
                body("status",equalTo(user.getStatus())).
                extract().path("id");
    }

    @AfterMethod
    public void deleteUser(){
        given().
                spec(requestSpec).
        when()
                .delete("/users/"+userId);
    }
}
