package com.check24.interviewTasks.tests.apiTests;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import static io.restassured.RestAssured.oauth2;

public class BaseTest {

    protected RequestSpecification requestSpec;
    protected static ResponseSpecification responseSpec;

    @BeforeClass
    public void createRequestSpecification() {

        requestSpec = new RequestSpecBuilder().
                setBaseUri("https://gorest.co.in/public/v2/").
                addFilter(new RequestLoggingFilter()).
                addFilter(new ResponseLoggingFilter()).
                //secret and sensitive informations such as token should be put inside .env files and not tracked in git
                        setAuth(oauth2("7141a2d79416e331164f64abc94dec2f5c68b49ebdbc507df0def2dc08edec43")).
                        setAccept(ContentType.JSON).
                        setContentType(ContentType.JSON).
                        build();
    }

    @BeforeClass
    public void createResponseSpecification() {

        responseSpec = new ResponseSpecBuilder().
                expectStatusCode(201).
                expectContentType(ContentType.JSON).
                build();
    }
}
