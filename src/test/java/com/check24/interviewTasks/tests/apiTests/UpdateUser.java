package com.check24.interviewTasks.tests.apiTests;

import com.check24.interviewTasks.apiModels.createUser.UserRequest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class UpdateUser extends BaseTest{
    UserRequest user;
    int userId;
    public UpdateUser() {
        this.user = UserRequest.builder()
                .name("User 04")
                .gender("male")
                .email("random04@email.com")
                .status("active")
                .build();
    }

    @BeforeClass
    public void beforeClassSetUp(){
        userId = given().
                spec(requestSpec).
                when().
                body(user).
                post("/users").
                then().
                statusCode(201).
                extract().path("id");
    }

    @Test
    public void updateUserName_checkResponseCode_expect200(){
        given().
                spec(requestSpec).
        when().
                body(UserRequest.builder()
                        .name("some name")
                        .email("some@email.com")
                        .gender("male").
                        status("active").build()).
                put("/users/"+userId).
        then().
                statusCode(200);
    }

    @AfterClass
    public void deleteUser(){
        given().
                spec(requestSpec).
                when()
                .delete("/users/"+userId);
    }
}
