package com.check24.interviewTasks.tests.apiTests;

import com.check24.interviewTasks.apiModels.createUser.UserRequest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class DeleteUser extends BaseTest{

    UserRequest user;
    int userId;
    public DeleteUser() {
        this.user = UserRequest.builder()
                .name("User 03")
                .gender("male")
                .email("random03@email.com")
                .status("active")
                .build();
    }

    @BeforeClass
    public void beforeClassSetUp(){
        userId = given().
                spec(requestSpec).
                when().
                body(user).
                post("/users").
                then().
                statusCode(201).
                extract().path("id");
    }

    @Test
    public void deleteUser_checkResponseCode_expect204(){
        given().
                spec(requestSpec).
        when().
                delete("/users/"+userId).
        then().
                statusCode(204);
    }

}
