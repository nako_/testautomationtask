package com.check24.interviewTasks.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePage {
    //region Locators
            private final By WechselnPkwRadioInput = By.id("wechseln_pkw");
            private final By TarifeVergleichenButton = By.xpath("//button[contains(text(),'Tarife vergleichen')]");
            private final By AkzeptierenButton = By.xpath("//a[contains(text(),'Akzeptieren')]");


    //endregion

    WebDriver driver;
    WebDriverWait wait;
    public HomePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    public void clickAkzeptierenButton(){

        wait.until(ExpectedConditions.elementToBeClickable(AkzeptierenButton)).click();
    }

    public void clickWechselnPkwRadioInput(){
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(WechselnPkwRadioInput).findElement(By.xpath("./.."))));
        driver.findElement(WechselnPkwRadioInput).click();
    }

    public EinsurancePkwBerechnenPage clickTarifeVergleichenButton(){
        wait.until(ExpectedConditions.elementToBeClickable(TarifeVergleichenButton)).click();
        return new EinsurancePkwBerechnenPage(driver);
    }
}