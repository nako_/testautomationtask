package com.check24.interviewTasks.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class EinsurancePkwBerechnenPage {
    //region Locators
//    private final By AndernSpan = By.id("span[id='c24_situation/SITUATION_SECTION_aendern']");
    private final By AndernSpan = By.id("c24_situation/SITUATION_SECTION_aendern");
    private final By WeiterButton = By.cssSelector("button[data-c24-test='opb-section-situation-submit']");
    private final By AktuelenVersicherungHochladenInput = By.id("c24_ftr_capture");
    private final By JetztHochladenUndAnalysierenButton = By.xpath("//button[contains(text(),'jetzt hochladen und analysieren')]");
    private final By NeinMitVorhandenemDokumentWeiterButton = By.xpath("//button[contains(text(),'nein, mit vorhandenem Dokument weiter')]");
    private final By OhneAnmeldungFortfahrenButton = By.xpath("//button[contains(text(),'ohne Anmeldung fortfahren')]");
    private final By WeitereDateiHochladenLabel = By.xpath("//span[contains(text(),'weitere Datei hochladen')]");
    private final By UploadedFileNamesDiv = By.cssSelector("div.css-1czcnll");
    private By UploadedFileNamesDivs(String text){ return By.xpath("//div[@class='css-1czcnll'][contains(text(),'"+text+"')]");}
    //endregion
    WebDriver driver;
    WebDriverWait wait;
    public EinsurancePkwBerechnenPage(WebDriver driver) {

        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    }

    public void clickAndernSpan(){
        wait.until(ExpectedConditions.elementToBeClickable(AndernSpan)).click();
    }

    public void clickWeiterButton(){
        wait.until(ExpectedConditions.elementToBeClickable(WeiterButton)).click();
    }

    public void uploadAktuelenVersicherungHochladenInput(String filePath){
            wait.until(ExpectedConditions.presenceOfElementLocated(AktuelenVersicherungHochladenInput)).sendKeys(filePath);
    }

    public void clickJetztHochladenUndAnalysierenButton(){
        wait.until(ExpectedConditions.elementToBeClickable(JetztHochladenUndAnalysierenButton)).click();
    }

    public void clickNeinMitVorhandenemDokumentWeiterButton(){
        wait.until(ExpectedConditions.elementToBeClickable(NeinMitVorhandenemDokumentWeiterButton)).click();
    }

    public void clickOhneAnmeldungFortfahrenButton(){
        wait.until(ExpectedConditions.elementToBeClickable(OhneAnmeldungFortfahrenButton)).click();
    }

    public String getUploadedFileNameElementText(){
        return wait.until(ExpectedConditions.visibilityOf(driver.findElement(UploadedFileNamesDiv))).getText();
    }

    public int getNumberOfSameNameUploadedFiles(String fileName){
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(UploadedFileNamesDivs(fileName))));
        return driver.findElements(UploadedFileNamesDivs(fileName)).size();
    }

    public boolean elementIsVisible(By selector){
        try {
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(selector)));
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
}