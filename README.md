**Check24 Test Automation task handover**

Note: Sometimes the UI test fails during the upload processing, but it is not a false negative, it is an actual bug where UI displays the following message "Es tut uns Leid, es ist ein technischer Fehler aufgetreten!"  

---

## How to run the tests

Note: A maven wrapper is added so maven install is not required to run the tests

1. Clone repository
2. Navigate to repository directory using the terminal of your choice
3. Execute command the following command: ***./mvnw clean test -Dsurefire.suiteXmlFiles=testng.xml***
4. Wait for tests to finish and 

---

## Overview of repo packages structure

- In the main directory a **resource** folder is created for saving any prerequisite files needed for testing
- Everything else is located under **src/test/java/com.check24.interviewTasks**
    - **apiModels** package holds the models or DTO for api request
    - **pages** package holds the page objects for the UI test
    - **tests** package holds 2 other packages where the tests are located 
        - **apiTests** in this package all the tests against the endpoints in the required list are located
        - **uiTests** in this package the tests against UI are located 
    
---